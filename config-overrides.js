const { addReactRefresh } = require('customize-cra-react-refresh');
const { override, addLessLoader } = require('customize-cra');

module.exports = {
  webpack: override(
    addLessLoader({
      lessOptions: {
        javascriptEnabled: true,
        localIdentName: '[local]--[hash:base64:5]',
        modifyVars: {
          'primary-color': '#003766',
          'link-color': '#0f5087',
          'input-height-base': '48px',
          'btn-height-base': '48px',
          '@height-base': '36px',
          '@height-sm': '28px',
          '@height-lg': '45px',
          '@border-radius-base': '4px',
        },
      },
    }),
    addReactRefresh(),
  ),
};
