/* eslint-disable */

importScripts('https://www.gstatic.com/firebasejs/8.0.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.0.1/firebase-messaging.js');

firebase.initializeApp({
  // Project Settings => Add Firebase to your web app
  apiKey: 'AIzaSyB3tpaevKYbkH4Mz57HMOdf6enjqEDtCaM',
  authDomain: 'siot-hust.firebaseapp.com',
  databaseURL: 'https://siot-hust.firebaseio.com',
  projectId: 'siot-hust',
  storageBucket: 'siot-hust.appspot.com',
  messagingSenderId: '408752237221',
  appId: '1:408752237221:web:b5e64978f7370c569d7b4c',
  measurementId: 'G-MZCRZSE7MG',
});

const messaging = firebase.messaging();
messaging.setBackgroundMessageHandler(function (payload) {
  console.log('payload: ', payload);

  // return self.registration.showNotification(payload);

  // DON'T SHOW ANYTHING
  return new Promise(function (resolve, reject) {});
});

self.addEventListener('notificationclick', function (event) {
  console.log('event: ', event);
  // do what you want
  // ...
});
