# SIOT Sphere

## Requirement:

- `Typescript` - Programming language
- `ReactJS` - HTML enhanced for web app!
- `Antd Design` - UI Library

## Quick start:

1. Create a .env file
2. Copy and edit your env from .env.example to .env
3. Make sure you already installed node in system. Start development:

```
$ npm install
$ npm start
```

### Need Support? Contact [Nguyễn Thành](https://facebook.com/thanhthanchuc)
