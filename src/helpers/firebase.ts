import firebase from 'firebase/app';

const firebaseConfig = {
  apiKey: 'AIzaSyB3tpaevKYbkH4Mz57HMOdf6enjqEDtCaM',
  authDomain: 'siot-hust.firebaseapp.com',
  databaseURL: 'https://siot-hust.firebaseio.com',
  projectId: 'siot-hust',
  storageBucket: 'siot-hust.appspot.com',
  messagingSenderId: '408752237221',
  appId: '1:408752237221:web:b5e64978f7370c569d7b4c',
  measurementId: 'G-MZCRZSE7MG',
};

export const getDeviceToken = async (): Promise<string> => {
  let token = '';

  try {
    const messaging = firebase.messaging();

    token = await messaging.getToken();
  } catch (error) {
    console.log(error);
  }

  return token;
};

export default firebaseConfig;
