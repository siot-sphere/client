export default function useValidateMessage() {
  return {
    required: 'This field is required!',
    types: {
      email: 'Invalid email format',
    },
  };
}
