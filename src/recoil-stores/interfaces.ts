export interface Authenticated {
  isLogin: boolean;
}

export interface CallingAPI {
  loading: boolean;
  response: any;
}

export interface Attribute {
  name: string;
  slug: string;
  type: string;
}

export interface DeviceInput {
  step: number;
  name: string;
  slug: string;
  image: string;
  files: any[];
  attributes: Attribute[];
  fileList: string[];
}
