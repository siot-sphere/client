import React, { memo } from 'react';
import { Col, Menu, Row, Modal } from 'antd';
//internal
import HeaderDropdown from '../HeaderDropdown';

import styles from './index.module.less';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import { useHistory } from 'react-router-dom';
import { AppRoute } from 'helpers';
import { useRecoilState } from 'recoil';
import { currentUser } from 'recoil-stores';

interface Props {}
const AvatarDropdown: React.SFC<Props> = memo((props) => {
  const [user] = useRecoilState(currentUser);

  const history = useHistory();

  const handleLogout = async () => {
    Modal.confirm({
      title: 'Confirm',
      icon: <ExclamationCircleOutlined />,
      content: 'Do you want to sign out of system?',
      onOk() {
        history.push(AppRoute.logout);
      },
    });
  };
  const menuHeaderDropdown = (
    <Menu className={styles.menu} selectedKeys={[]}>
      <Menu.Item key="users">
        <Row className={styles.blogProfile} gutter={18}>
          <Col span={8}>
            <div className="user-avatar">
              <div>
                {user?.firstName.slice(0, 1) || 'M'}
                {user?.lastName.slice(0, 1) || 'S'}
              </div>
            </div>
          </Col>
          <Col span={16}>
            <p className={styles.name}>
              {user?.firstName} {user?.lastName}
            </p>
            <p className={styles.tag}>
              <span className={styles.detail}>{user?.isAdmin ? 'Admin' : 'Normal'}</span>
            </p>
            <p className={styles.info}>{user?.email}</p>
          </Col>
        </Row>
      </Menu.Item>
      <Menu.Item key="logout" onClick={() => handleLogout()}>
        Log out
      </Menu.Item>
    </Menu>
  );

  return (
    <HeaderDropdown overlay={menuHeaderDropdown}>
      <span className={`${styles.action} ${styles.account}`}>
        <div className="user-avatar user-avatar-small">
          <div>
            {user?.firstName.slice(0, 1) || 'M'}
            {user?.lastName.slice(0, 1) || 'S'}
          </div>
        </div>
      </span>
    </HeaderDropdown>
  );
});
export default AvatarDropdown;
