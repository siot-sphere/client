import BasicLayout, { BasicLayoutProps as ProLayoutProps, MenuDataItem, Settings } from '@ant-design/pro-layout';
import React, { ReactNode } from 'react';
import { Link, NavLink } from 'react-router-dom';
import RightContent from './GlobalHeader/RightContent';
import {
  MenuOutlined,
  HomeOutlined,
  UserOutlined,
  FundOutlined,
  AreaChartOutlined,
  AndroidOutlined,
  BookOutlined,
} from '@ant-design/icons';
import { useLocalStorage } from 'react-use';
import styles from './index.module.less';
import { SiderMenuProps } from '@ant-design/pro-layout/lib/SiderMenu/SiderMenu';
import { useRecoilState } from 'recoil';
import { deviceList, currentUser } from 'recoil-stores';
import { AppRoute } from 'helpers';

export interface BasicLayoutProps extends ProLayoutProps {
  breadcrumbNameMap: {
    [path: string]: MenuDataItem;
  };
  settings: Settings;
  children?: ReactNode;
}

const Main: React.FC<BasicLayoutProps> = (props) => {
  const { children, settings } = props;
  const [collapsed, setCollapsed] = useLocalStorage('collapsed', window.localStorage.getItem('collapsed'));

  const [devices] = useRecoilState(deviceList);
  const [user] = useRecoilState(currentUser);

  function renderMenuHeader(logoDom: ReactNode, titleDom: ReactNode, props?: SiderMenuProps) {
    return (
      <Link to="/">
        <span>
          {logoDom}
          {titleDom}
        </span>
      </Link>
    );
  }

  const route = {
    routes: [
      {
        path: AppRoute.home,
        icon: <HomeOutlined />,
        name: 'My devices',
      },
      ...(user?.isAdmin
        ? [
            {
              path: AppRoute.users,
              icon: <UserOutlined />,
              name: 'User Management',
            },
            {
              path: AppRoute.banners,
              icon: <AreaChartOutlined />,
              name: 'Device Banner Management',
            },
            {
              path: AppRoute.apis,
              icon: <BookOutlined />,
              name: 'API References',
            },
          ]
        : []),
      {
        path: AppRoute.versions,
        icon: <AndroidOutlined />,
        name: 'Platform Version Management',
      },
      ...devices.map((d) => ({
        path: AppRoute.device_detail_ref(d.slug),
        icon: <FundOutlined />,
        name: d.name,
        children: [
          ...(d.attributes?.length
            ? d.attributes.map((attr) => ({
                path: '/devices/' + d.slug + '/attrs/' + attr.slug,
                icon: <FundOutlined />,
                name: attr.name,
              }))
            : []),
        ],
      })),
    ],
  };

  return (
    <BasicLayout
      disableMobile
      className={styles.customLayout}
      logo={<MenuOutlined style={{ fontSize: 25, color: 'white' }} />}
      menuHeaderRender={renderMenuHeader}
      menuItemRender={(item, defaultDom) => {
        return item.isUrl ? defaultDom : <NavLink to={item?.path ?? ''}>{defaultDom}</NavLink>;
      }}
      rightContentRender={(rightProps) => <RightContent rightProps={rightProps} {...settings} />}
      {...props}
      {...settings}
      collapsed={collapsed === 'true'}
      onCollapse={(val) => {
        setCollapsed(String(val));
      }}
      title={'HUST - SIoT'}
      fixSiderbar={true}
      fixedHeader={true}
      route={route}
      siderWidth={300}
    >
      <div style={{ padding: 15 }} className="w-full h-full">
        {children}
      </div>
    </BasicLayout>
  );
};

export default Main;
