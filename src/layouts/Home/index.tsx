import { Col, Layout, Row } from 'antd';
import React, { ReactNode } from 'react';
import RightContent from 'layouts/Main/GlobalHeader/RightContent';
import { Color } from 'utils';
import { Link } from 'react-router-dom';
import { AppRoute } from 'helpers';

export interface HomeLayoutProps {
  children: ReactNode;
}

const HomeLayout: React.SFC<HomeLayoutProps> = ({ children }) => {
  return (
    <Layout>
      <Layout.Header>
        <Row justify="space-between" align="middle">
          <Col>
            <Link to={AppRoute.home}>
              <h1 className="h1" style={{ color: Color.WHITE }}>
                MS
              </h1>
            </Link>
          </Col>
          <RightContent />
        </Row>
      </Layout.Header>
      <Layout.Content>
        <div className="container">{children}</div>
      </Layout.Content>
    </Layout>
  );
};

export default HomeLayout;
