import { Col, message, Row } from 'antd';
import React, { useState } from 'react';
import { Line } from 'react-chartjs-2';
import { useParams } from 'react-router-dom';
import { DeviceService } from 'services';
import { AttributeValue, Attribute as AttributeEntity } from 'types';
import { Color } from 'utils';
import moment from 'moment';
import _ from 'underscore';
import ChartDataLabels from 'chartjs-plugin-datalabels';
import { Spinner } from 'components';
import DateTimePicker from './DateTimePicker';

export interface OldDataChartProps {
  attribute: AttributeEntity;
}

const OldDataChart: React.SFC<OldDataChartProps> = ({ attribute }) => {
  const [values, setValues] = useState<AttributeValue[]>([]);

  const [loading, setLoading] = useState(false);

  const { deviceSlug, attributeSlug } = useParams<{ deviceSlug: string; attributeSlug: string }>();

  const handleDateTimeChange = async ([from, to]: [moment.Moment, moment.Moment]) => {
    try {
      setLoading(true);
      const { body } = await DeviceService.getAttributeValueFilter(deviceSlug, attributeSlug, {
        from: from.toISOString(),
        to: to.toISOString(),
      });

      setValues(body);
      setLoading(false);
    } catch (error) {
      message.error('Oops! Something went wrong!');
    }
  };

  const data = () => {
    const labels = values.map((l, index) => {
      if (values.length <= 10) {
        return moment(l.createdAt).format('DD/MM/YYYY HH:mm:ss');
      }

      return _.range(0, values.length, Math.floor(values.length / 10)).includes(index)
        ? moment(l.createdAt).format('DD/MM/YYYY HH:mm:ss')
        : '';
    });

    return {
      labels: labels,
      datasets: [
        {
          borderColor: Color.PRIMARY,
          backgroundColor: Color.PRIMARY,
          pointBorderColor: Color.PRIMARY,
          pointBackgroundColor: Color.PRIMARY,
          pointHoverBackgroundColor: Color.PRIMARY,
          pointHoverBorderColor: Color.PRIMARY,
          lineTension: 0,

          label: attribute?.name || '',
          data: values.map((l) => l.value),

          datalabels: {
            display: true,
            color: Color.PINK,
            backgroundColor: 'transparent',
            font: { weight: 'bold' },
            borderRadius: 8,
            borderWidth: 1,
            padding: 4,
            align: 'top',
            offset: 12,
            formatter: function (value: any, context: any) {
              return +value === Math.min(...values.map((l) => +l.value)) ||
                +value === Math.max(...values.map((l) => +l.value))
                ? `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',') + attribute?.dataLabel
                : '';
            },
          },
        },
      ],
    };
  };

  const options = {
    legend: {
      display: false,
    },
    responsive: true,
    elements: {
      line: {
        fill: false,
      },
    },
    tooltips: {
      callbacks: {
        title: function (tooltipItems: any, data: any) {
          return `Value: ${attribute?.dataLabel || ''}`;
        },
      },
    },
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
            min: Math.floor(Math.min(...[...values.map((v) => +v.value)]) * 0.2),
            max: Math.floor(Math.max(...[...values.map((v) => +v.value)]) * 1.2),
            callback: function (value: any) {
              return value + ' ' + attribute?.dataLabel;
            },
          },
        },
      ],
    },
  };

  return (
    <div>
      <div>Old data:</div>
      <Row style={{ margin: '12px 0px' }}>
        <Col>
          <DateTimePicker onDateTimeChange={handleDateTimeChange} />
        </Col>
      </Row>
      <div className="chart_item">
        <div className="chart_detail">
          <Spinner loading={loading}>
            <Line data={data} options={options} plugins={[ChartDataLabels]} />
          </Spinner>
        </div>
      </div>
    </div>
  );
};

export default OldDataChart;
