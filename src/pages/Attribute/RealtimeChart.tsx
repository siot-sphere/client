import React, { useState } from 'react';
import firebase from 'firebase/app';
import 'firebase/messaging';
import { AttributeValue, Attribute as AttributeEntity } from 'types';
import { Color } from 'utils';
import moment from 'moment';
import { Line } from 'react-chartjs-2';

export interface RealtimeChartProps {
  attribute?: AttributeEntity;
}

const RealtimeChart: React.SFC<RealtimeChartProps> = ({ attribute }) => {
  const [values, setValues] = useState<AttributeValue[]>([]);

  if (firebase.messaging.isSupported()) {
    firebase.messaging().onMessage((payload) => {
      if (payload?.data?.data) {
        const value = JSON.parse(payload.data.data) as AttributeValue;

        value.attributeId === attribute?.id && setValues([...values, value]);
      }
    });
  }

  const data = () => {
    return {
      labels: values.map((l) => moment(l.createdAt).format('hh:mm:ss')),
      datasets: [
        {
          borderColor: Color.PRIMARY,
          backgroundColor: Color.PRIMARY,
          pointBorderColor: Color.PRIMARY,
          pointBackgroundColor: Color.PRIMARY,
          pointHoverBackgroundColor: Color.PRIMARY,
          pointHoverBorderColor: Color.PRIMARY,

          label: attribute?.name || '',
          data: values.map((l) => l.value),

          datalabels: {
            formatter: () => '',
          },
        },
      ],
    };
  };

  const options = {
    legend: {
      display: false,
    },
    responsive: true,
    elements: {
      line: {
        fill: false,
      },
    },
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: false,
            min: Math.floor(Math.min(...[...values.map((v) => +v.value)]) * 0.2),
            max: Math.floor(Math.max(...[...values.map((v) => +v.value)]) * 1.2),
            callback: function (value: any) {
              return value + ' ' + attribute?.dataLabel;
            },
          },
        },
      ],
    },
  };

  return <Line data={data()} options={options} height={210} />;
};

export default RealtimeChart;
