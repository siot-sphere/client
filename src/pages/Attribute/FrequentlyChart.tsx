import { message } from 'antd';
import React, { useState } from 'react';
import { useParams } from 'react-router-dom';
import { DeviceService } from 'services';
import { Attribute, AttributeValue } from 'types';
import { Color } from 'utils';
import { Line } from 'react-chartjs-2';
import _ from 'underscore';
import DateTimePicker from './DateTimePicker';
import { Moment } from 'moment';
import { Spinner } from 'components';

export interface FrequentlyChartProps {
  attribute: Attribute;
}

const FrequentlyChart: React.SFC<FrequentlyChartProps> = ({ attribute }) => {
  const { deviceSlug, attributeSlug } = useParams<{ deviceSlug: string; attributeSlug: string }>();
  const [loading, setLoading] = useState(false);

  const [datas, setDatas] = useState<AttributeValue[]>([]);

  const handleChange = async ([from, to]: [Moment, Moment]) => {
    setLoading(true);
    try {
      const { body } = await DeviceService.getFrequentlyAttributeValue(
        deviceSlug,
        attributeSlug,
        from.toISOString(),
        to.toISOString(),
      );

      setDatas(body);
    } catch (error) {
      message.error('Oops! Something went wrong!');
    }
    setLoading(false);
  };

  const data = () => {
    const labels = _.range(
      Math.floor(Math.min(...datas.map((v) => +v.value))),
      Math.floor(Math.max(...datas.map((v) => +v.value))),
      1,
    );

    return {
      labels: labels.map((l) => `${l}`),
      datasets: [
        {
          borderColor: Color.PINK,
          backgroundColor: Color.PINK,
          pointBorderColor: Color.PINK,
          pointBackgroundColor: Color.PINK,
          pointHoverBackgroundColor: Color.PINK,
          pointHoverBorderColor: Color.PINK,

          label: attribute?.name || '',
          data: labels.map((l: any, i) => datas.find((v) => v.value === l)?.count || 0),

          datalabels: {
            formatter: () => '',
          },
        },
      ],
    };
  };

  const options = {
    legend: {
      display: false,
    },
    responsive: true,
    elements: {
      line: {
        fill: false,
      },
    },
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
            callback: function (value: any) {
              return value + ' times';
            },
          },
        },
      ],
      xAxes: [
        {
          ticks: {
            callback: function (value: any) {
              return value + ' ' + attribute?.dataLabel;
            },
          },
        },
      ],
    },
  };

  return (
    <div>
      <div style={{ marginBottom: 12 }}>Frequently data:</div>
      <div style={{ marginBottom: 12 }}>
        <DateTimePicker onDateTimeChange={handleChange} />
      </div>
      <Spinner loading={loading}>
        <div className="chart_item">
          <div className="chart_detail">
            <Line data={data()} options={options} />
          </div>
        </div>
      </Spinner>
    </div>
  );
};

export default FrequentlyChart;
