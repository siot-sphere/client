import { InboxOutlined } from '@ant-design/icons';
import { Button, Col, message, Modal, Row } from 'antd';
import Table, { ColumnsType } from 'antd/lib/table';
import Dragger from 'antd/lib/upload/Dragger';
import React, { useEffect, useState } from 'react';
import XLSX from 'xlsx';
import moment from 'moment';
import { DeviceService } from 'services';
import { useParams } from 'react-router-dom';

export interface ImportDataModalProps {}

const ImportDataModal: React.SFC<ImportDataModalProps> = () => {
  const [visible, setVisible] = useState(false);
  const [files, setFiles] = useState<any[]>([]);
  const [data, setDatas] = useState<any[]>([]);
  const [previewVisible, setPreviewVisible] = useState(false);
  const [loading, setLoading] = useState(false);

  const props = {
    fileList: files,
    name: 'file',
    multiple: false,
    accept:
      '.txt, .csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel, .xlsx, .xls',
    onChange: ({ fileList }: any) => {
      setFiles(fileList.slice(-1));
    },
    beforeUpload: (file: any) => {
      return false;
    },
  };

  useEffect(() => {
    const isNumber = function isNumber(value: any) {
      return typeof value === 'number' && isFinite(value);
    };

    const isNumberObject = function isNumberObject(n: any) {
      return Object.prototype.toString.apply(n) === '[object Number]';
    };

    const isCustomNumber = function isCustomNumber(n: any) {
      return isNumber(n) || isNumberObject(n);
    };

    function ExcelDateToJSDate(serial: any) {
      // @ts-ignore
      if (isCustomNumber(serial)) {
        const utc_days = Math.floor(serial - 25569);
        const utc_value = utc_days * 86400;
        const date_info = new Date(utc_value * 1000);

        const fractional_day = serial - Math.floor(serial) + 0.0000001;

        let total_seconds = Math.floor(86400 * fractional_day);

        const seconds = total_seconds % 60;

        total_seconds -= seconds;

        const hours = Math.floor(total_seconds / (60 * 60));
        const minutes = Math.floor(total_seconds / 60) % 60;

        return new Date(
          date_info.getFullYear(),
          date_info.getMonth(),
          date_info.getDate(),
          hours,
          minutes,
          seconds,
        ).toISOString();
      }

      return serial;
    }

    if (files.length) {
      const file = files[0].originFileObj;
      const fileReader = new FileReader();

      fileReader.onload = function (event) {
        const data = event.target?.result;

        const workbook = XLSX.read(data, {
          type: 'binary',
        });

        const rows = XLSX.utils.sheet_to_json(workbook.Sheets[workbook.SheetNames[0]]).map((r: any) => ({
          ...r,
          created_at: r.created_at ? ExcelDateToJSDate(r.created_at) : new Date().toISOString(),
        }));

        setDatas(rows);
      };

      fileReader.readAsBinaryString(file);
    }
  }, [files]);

  const columns: ColumnsType<any> = [
    {
      title: '',
      dataIndex: 'no',
      width: 70,
      render: (text, record, index) => <span>{index + 1}</span>,
    },
    {
      title: `Value`,
      dataIndex: 'value',
    },
    {
      title: 'Created date',
      dataIndex: 'created_at',
      render: (text, record) => (
        <span>{record?.created_at ? moment(record.created_at).format('DD/MM/YYYY HH:mm:ss') : ''}</span>
      ),
    },
  ];

  const { deviceSlug, attributeSlug } = useParams<{ deviceSlug: string; attributeSlug: string }>();

  const hanldeUpload = async () => {
    setLoading(true);
    try {
      await DeviceService.saveOldDeviceValues(
        deviceSlug,
        attributeSlug,
        data.map((d) => ({ value: d.value, createdAt: new Date(d.created_at).toISOString() })),
      );

      setVisible(false);
      setFiles([]);
      setDatas([]);

      message.success('Upload successfull!');
    } catch (error) {
      message.error('Oops! Something went wrong. Try later!');
    }
    setLoading(false);
  };

  return (
    <Row justify="end">
      <Col>
        <Button type="primary" onClick={() => setVisible(true)} style={{ height: 42, padding: '7px 15px' }}>
          Import excel
        </Button>
      </Col>
      <Modal visible={visible} onCancel={() => setVisible(false)} footer={null} maskClosable={true} closeIcon={<></>}>
        <Dragger {...props}>
          <p className="ant-upload-drag-icon">
            <InboxOutlined />
          </p>
          <p className="ant-upload-text">Click or drag file to this area to upload</p>
          <p className="ant-upload-hint">Accept only .svg, .txt, .xls and .xlsx file.</p>
          <p className="ant-upload-hint">
            {`A properly formatted file is a file that contains only one sheet and it's first line has format `}
            <span
              style={{ padding: 4, display: 'inline-block', backgroundColor: 'rgba(0, 0, 0, 0.25)' }}
            >{`"value", "created_at"`}</span>
          </p>
          <Row justify="center">
            <Col span={4}>
              <strong>*Format:</strong>
            </Col>
            <Col span={14}>
              <p className="ant-upload-hint" style={{ textAlign: 'left' }}>{`"value", "created_at"`}</p>
            </Col>
          </Row>
          <Row justify="center">
            <Col span={4}></Col>
            <Col span={14}>
              <p className="ant-upload-hint" style={{ textAlign: 'left' }}>{`"value 1", "2020-11-23 14:17:06"`}</p>
            </Col>
          </Row>
          <Row justify="center">
            <Col span={4}></Col>
            <Col span={14}>
              <p className="ant-upload-hint" style={{ textAlign: 'left' }}>{`"value 2", "2020-11-23"`}</p>
            </Col>
          </Row>
          <Row justify="center">
            <Col span={4}></Col>
            <Col span={14}>
              <p className="ant-upload-hint" style={{ textAlign: 'left' }}>{`"value 3", "2020-11-23T07:17:06.000Z"`}</p>
            </Col>
          </Row>
        </Dragger>
        {data.length ? (
          <div style={{ margin: '12px 0px' }}>
            {data.length} records will be save to server!
            <Row justify="end" gutter={[8, 0]}>
              <Col>
                <Button type="default" size="small" onClick={() => setPreviewVisible(true)}>
                  Preview
                </Button>
              </Col>
              <Col>
                <Button type="primary" size="small" onClick={hanldeUpload} loading={loading} disabled={loading}>
                  Upload
                </Button>
              </Col>
            </Row>
          </div>
        ) : null}
      </Modal>
      <Modal
        visible={previewVisible}
        maskClosable={true}
        onCancel={() => setPreviewVisible(false)}
        footer={null}
        closeIcon={<></>}
      >
        <Table rowKey={(record) => record?.id || ''} dataSource={data.slice(0, 50)} columns={columns} />
      </Modal>
    </Row>
  );
};

export default ImportDataModal;
