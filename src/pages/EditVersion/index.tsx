import { Spinner } from 'components';
import { VersionFormContainer } from 'containers';
import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { PlatformVersionService } from 'services';
import { PlatformVersionEntity } from 'types';
import { ErrorMessage } from 'utils';

export interface EditPlatformVersionProps {}

function EditPlatformVersion(props: EditPlatformVersionProps) {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState<PlatformVersionEntity>();

  const { id } = useParams<{ id: string }>();

  useEffect(() => {
    const fetch = async () => {
      setLoading(true);
      try {
        const data = await PlatformVersionService.getPlatformVersionById(+id);
        setData(data);
      } catch (error) {
        ErrorMessage();
      }
      setLoading(false);
    };

    fetch();
  }, [id]);

  return <Spinner loading={loading}>{data ? <VersionFormContainer platformVersion={data} /> : null}</Spinner>;
}

export default EditPlatformVersion;
