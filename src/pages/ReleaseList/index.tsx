import { Button, Col, Row, Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { PlatformVersionService } from 'services';
import { PlatformVersionEntity } from 'types';
import { ErrorMessage } from 'utils';
import moment from 'moment';
import { lowerCase } from 'lodash';

export interface ReleaseListProps {}

function ReleaseList(props: ReleaseListProps) {
  const [data, setData] = useState<PlatformVersionEntity[]>([]);
  const [loading, setLoading] = useState(false);
  const [page, setPage] = useState(1);
  const [total, setTotal] = useState(0);

  useEffect(() => {
    const fetch = async () => {
      setLoading(true);
      try {
        const [response, total] = await PlatformVersionService.getPlatformVersionPaging({ page, limit: 10 });
        setData([...data, ...response]);
        setTotal(total);
      } catch (error) {
        ErrorMessage();
      }
      setLoading(false);
    };

    fetch();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  return (
    <Row className="flex-col h-full">
      <Row justify="space-between">
        <Typography.Text className="text-2xl">SIoT Platform Release Version history</Typography.Text>
      </Row>
      <Col className="bg-white rounded p-4 shadow mt-8 flex-grow">
        <div>
          <div className="mb-4">
            <span className="font-semibold">Total {total} released versions</span>
          </div>
          {data.map((item) => (
            <Row key={item.id} className="mb-12">
              <Col span={24} className="bg-black-3 border text-center border-black-35 px-4 py-1 border-b-0">
                <span className="font-semibold">
                  SIoT Platform {lowerCase(item.releaseType)} version {item.name}
                </span>
              </Col>
              <Col span={24} className="bg-black-3 border border-black-35 px-4 py-1 border-b-0">
                <div dangerouslySetInnerHTML={{ __html: item.description }} />
              </Col>
              <Col span={4} className="bg-black-3 border border-black-35 text-center p-1 border-b-0 border-r-0">
                <span className="font-semibold">Release key</span>
              </Col>
              <Col span={4} className="bg-black-3 border border-black-35 text-center p-1 border-b-0 border-r-0">
                <span className="font-semibold">Release Date</span>
              </Col>
              <Col span={16} className="bg-black-3 border border-black-35 text-center p-1 border-b-0 border-r-0">
                <span className="font-semibold">Release Archives</span>
              </Col>
              <Col span={4} className="border border-black-35 p-1 px-4 border-r-0">
                <span>{item.releaseKey}</span>
              </Col>
              <Col span={4} className="border border-black-35 p-1 px-4 border-r-0">
                <span>{moment(item.createdAt).format('LL')}</span>
              </Col>
              <Col span={16} className="border border-black-35 p-1 px-4 border-r-0">
                {item.medias?.map((media) => (
                  <div key={media.id}>
                    <a href={media.filePath} target="_blank" rel="noopener noreferrer">
                      <span className="underline">{media.fileName}</span>
                    </a>
                  </div>
                ))}
              </Col>
            </Row>
          ))}
          {total !== data.length && (
            <Row justify="center">
              <Button type="link" onClick={() => setPage(page + 1)} loading={loading} disabled={loading}>
                Load more
              </Button>
            </Row>
          )}
        </div>
      </Col>
    </Row>
  );
}

export default ReleaseList;
