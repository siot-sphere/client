import React from 'react';
import SwaggerUI from 'swagger-ui-react';
import 'swagger-ui-react/swagger-ui.css';

export interface APIDocProps {}

export default function APIDoc(props: APIDocProps) {
  return (
    <div>
      <SwaggerUI url="/open-apis/index.yml" />
    </div>
  );
}
