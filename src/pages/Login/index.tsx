import React, { memo, useEffect } from 'react';
import { Helmet } from 'react-helmet';

import { LoginContainer } from 'containers';
import { AppRoute } from 'helpers';
import { useHistory } from 'react-router-dom';
import { useRecoilState } from 'recoil';
import { authenticated } from 'recoil-stores';
import { useQueryString } from 'hooks';

const LoginPage: React.SFC = memo(() => {
  const [{ isLogin }] = useRecoilState(authenticated);

  const history = useHistory();

  const { redirect } = useQueryString<{ redirect: string }>();

  useEffect(() => {
    if (isLogin) {
      history.push(redirect || AppRoute.home);
    }
  }, [history, isLogin, redirect]);

  return (
    <>
      <Helmet title="SIoT | Login" />
      <LoginContainer />
    </>
  );
});

export default LoginPage;
