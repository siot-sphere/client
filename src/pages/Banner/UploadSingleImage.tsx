import React from 'react';
import { message, Row, Typography, Upload } from 'antd';
import { useState } from 'react';
import { Spinner } from 'components';
import { FormInstance } from 'antd/lib/form';
import { UploadFileSVG } from 'icons';
import { UploadMedia, BannerService } from 'services';
import { Banner } from 'types';

const { Text } = Typography;

export interface VideoAndImageUploaderProps {
  form?: FormInstance;
  required?: boolean;
  defaultList?: any[];
  editabled?: boolean;
  fileList: Banner[];
  setFileList: (fileList: Banner[]) => void;
}

function VideoAndImageUploader({ fileList, setFileList }: VideoAndImageUploaderProps) {
  const [loading, setLoading] = useState(false);

  const uploadButton = (
    <Row
      className="w-48 h-48 border-black-8 rounded border border-dashed bg-black-3 text-center flex-col hover:border-black-35"
      align="middle"
      justify="center"
    >
      {loading ? (
        <Spinner height={104} />
      ) : (
        <>
          <UploadFileSVG />
          <Text strong className="text-black-65">
            Upload
          </Text>
        </>
      )}
    </Row>
  );

  async function beforeUpload(file: File) {
    setLoading(true);

    try {
      const media = await UploadMedia(file as any);

      const url = await media.ref.getDownloadURL();

      const data = await BannerService.save(url);

      setFileList([data.body, ...fileList]);
    } catch (error) {
      message.error('Oops! Error when uploading file. Try later!');
    }

    setLoading(false);

    return false;
  }

  return (
    <Upload
      // action={process.env.REACT_APP_UPLOAD_ENDPOINT}
      accept="image/*,video/mp4,video/x-m4v,video/*"
      showUploadList={false}
      beforeUpload={(file) => {
        beforeUpload(file);
        return false;
      }}
    >
      {uploadButton}
    </Upload>
  );
}

export default VideoAndImageUploader;
