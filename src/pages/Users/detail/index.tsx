import React, { FC, memo, useEffect, useState } from 'react';
import { Breadcrumb, Col, Row, Spin } from 'antd';
import { useParams } from 'react-router-dom';

import styles from './index.module.less';
import UserInfo from './components/UserInfo';
import { UserService } from 'services';
import { User } from 'types';
import DeviceList from './components/DeviceList';

const UserDetail: FC = memo(() => {
  const [loading, setLoading] = useState(true);
  const [user, setUser] = useState<User | undefined>();

  const { id } = useParams<{ id: string }>();

  useEffect(() => {
    const fetch = async () => {
      setLoading(true);
      try {
        const { body } = await UserService.getById(id);

        setUser(body);
      } catch (error) {}
      setLoading(false);
    };

    fetch();
  }, [id]);

  return (
    <div>
      <Spin spinning={loading}>
        <Row>
          <Col span={12}>
            <Breadcrumb separator=">">
              <Breadcrumb.Item onClick={() => {}} href="">
                <span className={styles.brHeader}>Users</span>
              </Breadcrumb.Item>
              <Breadcrumb.Item>{user?.firstName + ' ' + user?.lastName}</Breadcrumb.Item>
            </Breadcrumb>
          </Col>
        </Row>
        <div className={styles.info} style={{ margin: '30px 0px' }}>
          <Row>
            <Col span={24}>
              {user && (
                <>
                  <UserInfo user={user} />
                  <DeviceList devices={user.devices} />
                </>
              )}
            </Col>
          </Row>
        </div>
      </Spin>
    </div>
  );
});

export default UserDetail;
