import React from 'react';
import { useParams } from 'react-router-dom';

export interface DeviceInstructionProps {}

const DeviceInstruction: React.SFC<DeviceInstructionProps> = () => {
  const { id } = useParams<{ id: string }>();

  return <div>{id}</div>;
};

export default DeviceInstruction;
