import { Switch } from 'antd';
import React, { useEffect, useState } from 'react';
import { DeviceService } from 'services';
import { Device } from 'types';
import { ErrorMessage } from 'utils';

export interface ToggleAutoUpdatePlatformVersionProps {
  device?: Device;
}

function ToggleAutoUpdatePlatformVersion({ device }: ToggleAutoUpdatePlatformVersionProps) {
  const [checked, setChecked] = useState(!!device?.allowAutoUpdate);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setChecked(!!device?.allowAutoUpdate);
  }, [device]);

  const handleChange = async (value: boolean) => {
    setChecked(value);

    setLoading(true);
    try {
      await DeviceService.editDeviceOwner(device?.slug!, {
        allowAutoUpdate: value,
      });
    } catch (error) {
      ErrorMessage();
    }
    setLoading(false);
  };

  return (
    <Switch
      checked={checked}
      disabled={!device?.currentPlatformVersionId || loading}
      loading={loading}
      onChange={(value) => handleChange(value)}
    />
  );
}

export default ToggleAutoUpdatePlatformVersion;
