import React from 'react';
import { Button, Modal, Row, Space, Switch, Table, Tooltip } from 'antd';
import moment from 'moment';
import styles from './index.module.less';
import { ColumnsType } from 'antd/lib/table';
import { Attribute as AttributeEntity, Device } from 'types';
import { Link, NavLink, useParams } from 'react-router-dom';
import { AppRoute } from 'helpers';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { DeviceService } from 'services';
import { useRecoilState } from 'recoil';
import { currentUser } from 'recoil-stores';
import { ErrorMessage } from 'utils';

const UserView = ({ attributes, device }: { attributes: AttributeEntity[]; device?: Device }) => {
  const { slug } = useParams<{ slug: string }>();

  const handleDeleteAttribute = async (id?: number) => {
    id &&
      Modal.confirm({
        title: 'Confirm',
        content: `Are you sure you want to delete this attribute? This action also delete all this attribute's datas!`,
        onOk: async () => {
          try {
            await DeviceService.deleteAttribute(id);
            window.location.reload();
          } catch (error) {
            ErrorMessage();
          }
        },
      });
  };

  const handleDeactive = async (attribute: AttributeEntity) => {
    if (attribute.isActive) {
      Modal.confirm({
        title: 'Confirm',
        content: `Are you sure you want to deactive this attribute? This action will block all incomming HTTP POST for insert this attribute's data in the future!`,
        onOk: async () => {
          try {
            await DeviceService.updateAttribute({ ...attribute, isActive: false });
            window.location.reload();
          } catch (error) {
            ErrorMessage();
          }
        },
      });
    } else {
      try {
        await DeviceService.updateAttribute({ ...attribute, isActive: true });
        window.location.reload();
      } catch (error) {
        ErrorMessage();
      }
    }
  };

  const [current] = useRecoilState(currentUser);

  const owner = current?.id === device?.userId;

  console.log(current, device);

  const columns: ColumnsType<AttributeEntity> = [
    {
      title: 'No',
      dataIndex: 'no',
      width: 70,
      render: (text, record, index) => <span>{index + 1}</span>,
    },
    {
      title: 'Id',
      dataIndex: 'id',
    },
    {
      title: 'Name',
      dataIndex: 'name',
      render: (text, record) => <Link to={AppRoute.attribute_ref(slug, record.slug)}>{record.name}</Link>,
    },
    {
      title: 'Slug Url',
      dataIndex: 'slug',
    },
    {
      title: 'Data type',
      dataIndex: 'dataType',
    },
    {
      title: 'Data label',
      dataIndex: 'dataLabel',
    },
    {
      title: 'Last active',
      dataIndex: 'lastActive',
      render: (text, record) => <span>{record?.lastActive ? moment(record.lastActive).format('DD/MM/yyyy') : ''}</span>,
    },
    {
      title: 'Created date',
      dataIndex: 'created_at',
      render: (text, record) => <span>{record?.createdAt ? moment(record.createdAt).format('DD/MM/yyyy') : ''}</span>,
    },
    {
      title: 'Status',
      dataIndex: 'status',
      render: (text, record) => <Switch checked={record.isActive} onChange={() => handleDeactive(record)} />,
    },
    {
      title: 'Actions',
      dataIndex: 'action',
      width: 100,
      render: (text, record) =>
        !owner ? null : (
          <Space>
            <Tooltip title="Edit">
              <NavLink
                to={{
                  pathname: AppRoute.edit_attribute_ref(slug, record.slug),
                }}
              >
                <Button icon={<EditOutlined />} />
              </NavLink>
            </Tooltip>
            <Tooltip title="Delete">
              <Button icon={<DeleteOutlined />} onClick={() => handleDeleteAttribute(record.id)} />
            </Tooltip>
          </Space>
        ),
    },
  ];

  return (
    <div className={styles.table}>
      {owner && (
        <Row justify="end" style={{ margin: 24 }}>
          You have {attributes.length} attributes. <Link to={AppRoute.new_attribute_ref(slug)}>Create new?</Link>
        </Row>
      )}
      <Table
        rowKey={(record) => record?.id || ''}
        dataSource={attributes}
        columns={columns}
        pagination={{
          current: 1,
          pageSize: 10,
          total: 0,
          position: ['bottomCenter'],
          showSizeChanger: false,
        }}
      />
    </div>
  );
};

export default UserView;
