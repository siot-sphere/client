import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Col, Form, Input, Row } from 'antd';
import { DeviceCard, Dropdown } from 'components';
import React, { useState } from 'react';
import { AttributeDataType } from 'types';
import { DeviceService, SlugService } from 'services';
import { useRecoilState, useResetRecoilState } from 'recoil';
import { deviceInput, deviceList } from 'recoil-stores';
import { useHistory } from 'react-router-dom';
import { AppRoute } from 'helpers';
import { ErrorMessage } from 'utils';

export interface AttributesProps {}

const Attributes: React.SFC<AttributesProps> = () => {
  const [loading, setLoading] = useState(false);

  const form = Form.useForm()[0];

  const [, setDevices] = useRecoilState(deviceList);
  const [{ name, image, step, attributes, slug }, setState] = useRecoilState(deviceInput);
  const reset = useResetRecoilState(deviceInput);

  const history = useHistory();

  const handleSubmit = async () => {
    setState((state) => ({ ...state, attributes: form.getFieldsValue().attributes }));
    setLoading(true);

    try {
      const data = await DeviceService.save({
        name,
        image,
        slug,
        attributes: form
          .getFieldsValue()
          .attributes.map(({ name, slug, type, label }: any) => ({ name, slug, dataType: type, dataLabel: label })),
      });

      setDevices((devices) => [...devices, data.body]);
      history.push(AppRoute.device_detail_ref(data.body.slug));
      reset();
      window.location.reload();
    } catch (error) {
      ErrorMessage();
    }

    setLoading(false);
  };

  const validateSlug = () => ({
    async validator(rule: any, value: any) {
      if (value) {
        if (form.getFieldsValue().attributes.filter(({ name, slug, type }: any) => slug === value).length !== 1) {
          return Promise.reject('Slug has been used');
        }

        try {
          const {
            body: { verify },
          } = await SlugService.verify(value);

          if (!verify) {
            return Promise.reject('Slug has been used');
          }
        } catch (error) {
          //
        }
      }

      return Promise.resolve();
    },
  });

  const handleFieldsChange = async (fields: any) => {
    const field = fields[0];

    if (field && field.name[0] === 'attributes' && field.name[2] === 'name') {
      try {
        const {
          body: { slug },
        } = await SlugService.suggest({ name: field.value, type: 'ATTRIBUTE' });

        const { attributes: attr } = form.getFieldsValue();

        form.setFieldsValue({
          attributes: [...attr.map((attr: any, index: any) => (index === field.name[1] ? { ...attr, slug } : attr))],
        });
      } catch (error) {
        //
      }
    }
  };

  return (
    <div>
      <Form
        name="attribute-form"
        form={form}
        initialValues={{ attributes }}
        onFieldsChange={handleFieldsChange}
        onFinish={handleSubmit}
      >
        <Row className="device-form_form-group">
          <Col span={14}>
            <Col span={24} className="form-label">
              <h2 className="h2">{`Add your device attribute (eg: Temperature, Humidity, ...)`}</h2>
            </Col>
            <div style={{ marginTop: 24 }}>
              <Form.List name="attributes">
                {(fields, { add, remove }) => {
                  return (
                    <div>
                      {fields.map((field, index) => (
                        <Form.Item required={false} key={field.key}>
                          <Row align="middle" gutter={[8, 8]}>
                            <Col span={6}>
                              <Form.Item
                                {...field}
                                name={[field.name, 'name']}
                                fieldKey={[field.fieldKey, 'name']}
                                validateTrigger={['onChange']}
                                rules={[
                                  {
                                    required: true,
                                    whitespace: true,
                                    message: "Please input attribute's name or delete this field.",
                                  },
                                ]}
                                noStyle
                              >
                                <Input placeholder="Name" />
                              </Form.Item>
                            </Col>
                            <Col span={6}>
                              <Form.Item
                                {...field}
                                name={[field.name, 'slug']}
                                fieldKey={[field.fieldKey, 'slug']}
                                validateTrigger={['onChange']}
                                rules={[
                                  {
                                    required: true,
                                    whitespace: true,
                                    message: 'Missing slug name!',
                                  },
                                  { whitespace: false },
                                  {
                                    pattern: new RegExp('^([a-zA-Z0-9\\u3131-\\uD79D-_.])*$'),
                                    message: 'Slug name cannot have white space',
                                  },
                                  validateSlug,
                                ]}
                                noStyle
                              >
                                <Input placeholder="Slug name" />
                              </Form.Item>
                            </Col>

                            <Col span={5}>
                              <Form.Item
                                {...field}
                                name={[field.name, 'type']}
                                fieldKey={[field.fieldKey, 'type']}
                                rules={[{ required: true, message: 'Missing attribute type' }]}
                                noStyle
                              >
                                <Dropdown
                                  options={[
                                    ...Object.keys(AttributeDataType).map((attr) => ({ name: attr, code: attr })),
                                  ]}
                                  placeholder="Type"
                                />
                              </Form.Item>
                            </Col>
                            <Col span={5}>
                              <Form.Item
                                {...field}
                                name={[field.name, 'label']}
                                fieldKey={[field.fieldKey, 'label']}
                                validateTrigger={['onChange']}
                                rules={[
                                  {
                                    required: true,
                                    whitespace: true,
                                    message: 'Missing data label name!',
                                  },
                                  { whitespace: false },
                                ]}
                                noStyle
                              >
                                <Input placeholder="Data label: *C, PH, ..." />
                              </Form.Item>
                            </Col>
                            <Col span={2}>
                              {fields.length ? (
                                <MinusCircleOutlined
                                  className="dynamic-delete-button"
                                  style={{ margin: '0 8px', fontSize: 24 }}
                                  onClick={() => {
                                    remove(field.name);
                                  }}
                                />
                              ) : null}
                            </Col>
                          </Row>
                        </Form.Item>
                      ))}
                      {fields.length < 6 ? (
                        <Form.Item>
                          <Button
                            type="dashed"
                            onClick={() => {
                              add();
                            }}
                            style={{ width: '80%' }}
                          >
                            <PlusOutlined /> Add attribute
                          </Button>
                        </Form.Item>
                      ) : null}
                    </div>
                  );
                }}
              </Form.List>
            </div>
          </Col>

          <Col span={8}>
            <DeviceCard device={{ image, name, slug }} allowAction={false} />
          </Col>

          <Col span={24}>
            <Row className="device-form_button-action">
              <Col span={12} className="prev-button">
                <Button type="text" onClick={() => setState((state) => ({ ...state, step: step - 1 }))}>
                  Previous
                </Button>
              </Col>
              <Col span={12} className="next-button">
                <Button type="primary" htmlType="submit" loading={loading} disabled={loading}>
                  Continue
                </Button>
              </Col>
            </Row>
          </Col>
        </Row>
      </Form>
    </div>
  );
};

export default Attributes;
