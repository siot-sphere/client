import { CloseOutlined } from '@ant-design/icons';
import { AppRoute } from 'helpers';
import React from 'react';
import { Link } from 'react-router-dom';
import Name from './Name';
import Avatar from './Avatar';
import Attributes from './Attribute';
import { useRecoilState } from 'recoil';
import { deviceInput } from 'recoil-stores';

export interface DeviceFormProps {}

const DeviceForm: React.SFC<DeviceFormProps> = () => {
  const [{ step }] = useRecoilState(deviceInput);

  return (
    <div className="device-form-container">
      <div className="device-form_header">
        <h3 className="h3">
          <Link to={AppRoute.home}>
            <CloseOutlined />
          </Link>
          Register a device (Step {step} of 3)
        </h3>
      </div>

      {step === 1 && <Name />}
      {step === 2 && <Avatar />}
      {step === 3 && <Attributes />}
    </div>
  );
};

export default DeviceForm;
