import React, { memo, SFC } from 'react';
import { Button } from 'antd';

import styles from '../styles/index.module.less';

const Error500: SFC = memo(() => {
  const goToHomePage = (e: React.MouseEvent) => {
    e.preventDefault();
  };
  return (
    <div className={styles.iso404Page}>
      <div className={styles.iso404Content}>
        <h1>500</h1>
        <h3>Internal Server Error</h3>
        <p>Something went wrong. Please try again later.</p>
        <Button type="primary" onClick={goToHomePage} shape="round" style={{ marginTop: 15 }}>
          Back home
        </Button>
      </div>
      <div className={styles.iso404Artwork}>
        <img alt="500" src="#" />
      </div>
    </div>
  );
});

export default Error500;
