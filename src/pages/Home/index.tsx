import { PlusOutlined } from '@ant-design/icons';
import { Col, Row } from 'antd';
import { DeviceCard } from 'components';
import { AppRoute } from 'helpers';
import React from 'react';
import { Helmet } from 'react-helmet';
import { Link, useHistory } from 'react-router-dom';
import { useRecoilState } from 'recoil';
import { deviceList } from 'recoil-stores';
import { Color } from 'utils';

const HomePage: React.SFC = React.memo(() => {
  const [devices] = useRecoilState(deviceList);

  const history = useHistory();

  return (
    <>
      <Helmet title="SIoT - Home management" />
      <div className="home-container">
        <div className="device-list">
          <div className="device-list_title">
            <h4 className="h4">Your devices</h4>
          </div>
          <div className="device-list-card">
            <Row gutter={[32, 24]}>
              <Col span={8} onClick={() => history.push(AppRoute.new_device)}>
                <Row className="device-list-card_item flex-column" justify="center" align="middle">
                  <PlusOutlined style={{ fontSize: 24, color: Color.PRIMARY }} />
                  <h4 style={{ color: Color.PRIMARY }}>Register New Device</h4>
                </Row>
              </Col>

              {devices.map((device) => (
                <Col span={8} key={device.id}>
                  <Link to={'/devices/' + device.slug}>
                    <DeviceCard device={device} />
                  </Link>
                </Col>
              ))}
            </Row>
          </div>
        </div>
      </div>
    </>
  );
});

export default HomePage;
