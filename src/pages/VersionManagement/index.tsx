import { Button, Col, message, Modal, Row, Tooltip, Typography } from 'antd';
import { AppRoute } from 'helpers';
import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { PlatformVersionService } from 'services';
import { PlatformVersionEntity } from 'types';
import { ErrorMessage } from 'utils';
import moment from 'moment';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { lowerCase } from 'lodash';
import { useRecoilState } from 'recoil';
import { currentUser } from 'recoil-stores';
import { AppInput } from 'components';
import { useNavigate, useQueryString } from 'hooks';

export interface VersionManagementProps {}

function VersionManagement(props: VersionManagementProps) {
  const [data, setData] = useState<PlatformVersionEntity[]>([]);
  const [loading, setLoading] = useState(false);
  const [page, setPage] = useState(1);
  const [total, setTotal] = useState(0);

  const { search } = useQueryString<{ search: string }>();

  const [searchString, setSearchString] = useState(search || '');

  useEffect(() => {
    const fetch = async () => {
      setLoading(true);
      try {
        const [response, total] = await PlatformVersionService.getPlatformVersionPaging({ page, limit: 10 }, search);
        setData([...data, ...response]);
        setTotal(total);
      } catch (error) {
        ErrorMessage();
      }
      setLoading(false);
    };

    fetch();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page, search]);

  const [user] = useRecoilState(currentUser);

  const navigate = useNavigate<{ search: string }>();

  return (
    <Row className="flex-col h-full">
      <Row justify="space-between">
        <Typography.Text className="text-2xl">Platform Version Release Version history</Typography.Text>

        <Link to={AppRoute.new_versions}>
          <Button type="primary">Add new Platform Version</Button>
        </Link>
        <Col span={24}>
          {user?.isAdmin && (
            <AppInput
              placeholder="Filter release owner by email"
              style={{ width: 400 }}
              value={searchString}
              onChange={(e) => setSearchString(e.target.value)}
              onPressEnter={() => navigate({ search: searchString })}
            />
          )}
        </Col>
      </Row>
      <Col className="bg-white rounded p-4 shadow mt-8 flex-grow">
        <div>
          <div className="mb-4">
            <span className="font-semibold">Total {total} released versions</span>
          </div>
          {data.map((item) => (
            <Row key={item.id} className="mb-12">
              <Col span={24} className="bg-black-3 border text-center border-black-35 px-4 py-1 border-b-0">
                <span className="font-semibold">
                  Platform {lowerCase(item.releaseType)} version {item.name}
                </span>
              </Col>
              <Col span={24} className="bg-black-3 border border-black-35 px-4 py-1 border-b-0">
                <div dangerouslySetInnerHTML={{ __html: item.description }} />
              </Col>
              <Col span={3} className="bg-black-3 border border-black-35 text-center p-1 border-b-0 border-r-0">
                <span className="font-semibold">Version key</span>
              </Col>
              <Col span={3} className="bg-black-3 border border-black-35 text-center p-1 border-b-0 border-r-0">
                <span className="font-semibold">Created Date</span>
              </Col>
              <Col
                span={user?.isAdmin ? 11 : 14}
                className="bg-black-3 border border-black-35 text-center p-1 border-b-0 border-r-0"
              >
                <span className="font-semibold">Version Archives</span>
              </Col>
              {user?.isAdmin && (
                <Col span={4} className="border border-black-35 p-1 px-4 border-r-0">
                  <span>Owner</span>
                </Col>
              )}
              <Col span={3} className="bg-black-3 border border-black-35 text-center p-1 border-b-0">
                <span className="font-semibold">Action</span>
              </Col>
              <Col span={3} className="border border-black-35 p-1 px-4 border-r-0">
                <span>{item.releaseKey}</span>
              </Col>
              <Col span={3} className="border border-black-35 p-1 px-4 border-r-0">
                <span>{moment(item.createdAt).format('LL')}</span>
              </Col>
              <Col span={user?.isAdmin ? 11 : 14} className="border border-black-35 p-1 px-4 border-r-0">
                {item.medias?.map((media) => (
                  <div key={media.id}>
                    <a href={media.filePath} target="_blank" rel="noopener noreferrer">
                      <span className="underline">{media.fileName}</span>
                    </a>
                  </div>
                ))}
              </Col>
              {user?.isAdmin && (
                <Col span={4} className="border border-black-35 p-1 px-4 border-r-0">
                  <span>{item.user?.email}</span>
                </Col>
              )}
              <Col span={3} className="border border-black-35 text-center p-1">
                <Row align="middle" justify="center">
                  <Tooltip placement="top" title={'Edit'}>
                    <div className="mr-2 cursor-pointer">
                      <Link to={AppRoute.edit_version_ref(item.id!)}>
                        <EditOutlined style={{ width: 32, height: 32 }} className="p-2 bg-blue-8 rounded" />
                      </Link>
                    </div>
                  </Tooltip>
                  <Tooltip placement="top" title={'Delete'}>
                    <div
                      className="cursor-pointer"
                      onClick={() => {
                        Modal.confirm({
                          title: 'Are you sure you want to delete this released version?',
                          okText: 'Confirm',
                          onOk: async () => {
                            try {
                              await PlatformVersionService.deleteVersionById(item.id!);
                              message.success('Version deleted!');
                              setData(data.filter((d) => d.id !== item.id));
                              setTotal(total - 1);
                            } catch (error) {
                              ErrorMessage();
                            }
                          },
                        });
                      }}
                    >
                      <DeleteOutlined className="p-2 bg-black-3 rounded" style={{ width: 32, height: 32 }} />
                    </div>
                  </Tooltip>
                </Row>
              </Col>
            </Row>
          ))}
          {total !== data.length && (
            <Row justify="center">
              <Button type="link" onClick={() => setPage(page + 1)} loading={loading} disabled={loading}>
                Load more
              </Button>
            </Row>
          )}
        </div>
      </Col>
    </Row>
  );
}

export default VersionManagement;
