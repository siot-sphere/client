import superagent from 'superagent';
import SuperagentPrefix from 'superagent-prefix';
import { PlatformVersionEntity, PlatformVersionReleaseEnum, QueryPagination } from 'types';

const prefix = SuperagentPrefix(process.env.REACT_APP_API_ENDPOINT + '/api/platforms');

export interface CreateNewPlatformInput {
  description: string;
  name: string;
  releaseType: PlatformVersionReleaseEnum;
  medias: {
    fileName: string;
    filePath: string;
  }[];
}

async function createNewPlatformVersion(platform: PlatformVersionEntity) {
  const token = localStorage.getItem('accessToken');

  return (await superagent.post('/').use(prefix).set('Authorization', `Bearer ${token}`).send(platform))
    .body as Promise<PlatformVersionEntity>;
}

async function getPlatformVersionPaging(query: QueryPagination, search?: string) {
  const token = localStorage.getItem('accessToken');

  return (
    await superagent
      .get('/')
      .use(prefix)
      .set('Authorization', `Bearer ${token}`)
      .query({ ...query, search })
  ).body as Promise<[PlatformVersionEntity[], number]>;
}

async function getPlatformVersionById(id: number) {
  const token = localStorage.getItem('accessToken');

  return (
    await superagent
      .get('/' + id)
      .use(prefix)
      .set('Authorization', `Bearer ${token}`)
  ).body as Promise<PlatformVersionEntity>;
}

async function editPlatformVersionById(id: number, payload: PlatformVersionEntity) {
  const token = localStorage.getItem('accessToken');
  return (
    await superagent
      .put('/' + id)
      .use(prefix)
      .set('Authorization', `Bearer ${token}`)
      .send(payload)
  ).body as Promise<PlatformVersionEntity>;
}

async function deleteVersionById(id: number) {
  const token = localStorage.getItem('accessToken');
  return await superagent
    .delete('/' + id)
    .use(prefix)
    .set('Authorization', `Bearer ${token}`);
}

async function verifyReleaseKey(value: string) {
  const token = localStorage.getItem('accessToken');
  return (await (
    await superagent.post('/verify-key').use(prefix).send({ value }).set('Authorization', `Bearer ${token}`)
  ).body) as Promise<Boolean>;
}

export default {
  createNewPlatformVersion,
  getPlatformVersionPaging,
  getPlatformVersionById,
  editPlatformVersionById,
  deleteVersionById,
  verifyReleaseKey,
};
