import React, { useState } from 'react';
// @ts-ignore
import CKEditor from 'ckeditor4-react';
import { FormInstance } from 'antd/lib/form';

interface EditorProps {
  form: FormInstance;
  initValue?: string;
}

function MyEditor({ form, initValue = '' }: EditorProps) {
  const [data, setData] = useState(initValue);

  function handleChange(changeEvent: any) {
    setData(changeEvent.editor.getData());
    form.setFieldsValue({
      description: changeEvent.editor.getData(),
    });
  }

  return (
    <CKEditor
      data={data}
      onChange={handleChange}
      config={{
        toolbar: [
          ['Source'],
          ['Link'],
          ['NumberedList', 'BulletedList'],
          ['HorizontalRule'],
          ['Mode'],
          ['Styles', 'Format', 'Font', 'FontSize'],
          ['Bold', 'Italic', 'Subscript', 'Superscript'],
          ['Undo', 'Redo'],
        ],
        language: 'en',
      }}
    />
  );
}

export default MyEditor;
