import React, { useEffect, useState } from 'react';
import { Upload, Button, message, Typography } from 'antd';
import { DeleteOutlined, UploadOutlined } from '@ant-design/icons';
import { UploadMedia } from 'services';
import { PlatformMediaEntity } from 'types';
import { FormInstance } from 'antd/lib/form';

export interface UploadFileProps {
  form: FormInstance;
  defaultFileList?: PlatformMediaEntity[];
}

function UploadFile({ form, defaultFileList = [] }: UploadFileProps) {
  const [loading, setLoading] = useState(false);
  const [fileList, setFileList] = useState<PlatformMediaEntity[]>(defaultFileList);

  useEffect(() => {
    form.setFieldsValue({ files: fileList });
  }, [fileList, form]);

  async function beforeUpload(file: File) {
    setLoading(true);

    try {
      const media = await UploadMedia(file as any);

      const filePath = await media.ref.getDownloadURL();

      setFileList([...fileList, { fileName: file.name, filePath }]);
    } catch (error) {
      message.error('Oops! Error when uploading file. Try later!');
    }

    setLoading(false);

    return false;
  }

  return (
    <div>
      <Upload
        beforeUpload={(file) => {
          beforeUpload(file);
          return false;
        }}
        showUploadList={false}
        disabled={loading}
      >
        <Button icon={<UploadOutlined />} loading={loading} disabled={loading}>
          Upload
        </Button>
      </Upload>
      {fileList.map((f) => (
        <div key={f.filePath}>
          <a href={f.filePath} target="_blank" rel="noreferrer noopener">
            <Typography.Text className="text-blue underline">{f.fileName}</Typography.Text>
          </a>
          <Button
            type="link"
            className="h-10"
            onClick={() => setFileList([...fileList.filter((item) => item.filePath !== f.filePath)])}
          >
            <DeleteOutlined />
          </Button>
        </div>
      ))}
    </div>
  );
}

export default UploadFile;
