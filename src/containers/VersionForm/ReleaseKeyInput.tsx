import React from 'react';
import { AppInput, FormItem } from 'components';
import { PlatformVersionService } from 'services';
import { PlatformVersionEntity } from 'types';

export interface ReleaseKeyInputProps {
  disabled?: boolean;
  platformVersion?: PlatformVersionEntity;
}

function ReleaseKeyInput({ disabled, platformVersion }: ReleaseKeyInputProps) {
  const validator: any = async (rule: any, value: string) => {
    if (platformVersion?.releaseKey === value) {
      return Promise.resolve();
    }

    try {
      const data = await PlatformVersionService.verifyReleaseKey(value);

      return data ? Promise.resolve() : Promise.reject('This release key already used!');
    } catch (error) {
      return Promise.reject('This release key already used!');
    }
  };

  return (
    <FormItem
      name="releaseKey"
      validateFirst
      hasFeedback
      rules={[
        { required: true },
        {
          pattern: new RegExp(/^[a-zA-Z0-9_.-]*$/),
          message: `Only letter, number, '_', '.' and '-' is allowed!`,
        },
        { validator },
      ]}
    >
      <AppInput placeholder="Release Key" disabled={disabled} />
    </FormItem>
  );
}

export default ReleaseKeyInput;
