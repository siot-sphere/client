import React from 'react';
import { Input } from 'antd';
import { FormItem } from 'components';

const PasswordConfirmation = () => {
  const [completePassword, setCompletePassword] = React.useState(false);
  const [password, setPassword] = React.useState('');

  return (
    <>
      <FormItem
        name="password-confirmation"
        hasFeedback
        rules={[
          { required: true },
          ({ getFieldValue }) => ({
            validator(rule, value) {
              setPassword(value);

              if (getFieldValue('password') === value) setCompletePassword(true);
              else setCompletePassword(false);

              if (!value || getFieldValue('password') === value) {
                return Promise.resolve();
              }
              return Promise.reject('Your password confirm not match!');
            },
          }),
        ]}
        validateStatus={password && completePassword ? 'success' : undefined}
      >
        <Input type="password" placeholder="Retype your password" className="form-control" />
      </FormItem>
    </>
  );
};

export default PasswordConfirmation;
