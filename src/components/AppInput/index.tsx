import React from 'react';
import { Input } from 'antd';
import { InputProps } from 'antd/lib/input';

export interface AppInputProps extends InputProps {}

function AppInput({ size = 'large', ...props }: AppInputProps) {
  return <Input size={size} {...props} className={'text-sm py-4 ' + props.className} />;
}

export default AppInput;
