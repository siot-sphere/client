import React from 'react';
import Form, { FormItemProps } from 'antd/lib/form';

interface PropTypes extends FormItemProps {
  required?: boolean;
}

const FormItem: React.SFC<PropTypes> = ({ required, children, rules, ...props }) => {
  return (
    <Form.Item rules={[...(rules || []), { required }]} {...props} normalize={(value: string) => value.trimLeft()}>
      {children}
    </Form.Item>
  );
};

export default FormItem;
