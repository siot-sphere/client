import React, { useState } from 'react';
import { Cascader } from 'antd';
import { CascaderProps, CascaderOptionType } from 'antd/lib/cascader';

interface Option {
  code: string | number | any;
  name: string;
  items?: Option[] | CascaderOptionType[];
}

export interface CustomDropdownProps extends CascaderProps {
  className?: string;
  options: Option[] | CascaderOptionType[];
  defaultValue?: string[];
  allowClear?: boolean;
  suffixIcon?: any;
  expandIcon?: any;
}

const CustomDropdown: React.SFC<CustomDropdownProps> = ({
  placeholder,
  onChange,
  className,
  options,
  defaultValue,
  allowClear,
  suffixIcon,
  expandIcon,
  dropdownRender,
  ...props
}) => {
  const [visiable, setVisiable] = useState(false);

  return (
    <Cascader
      defaultValue={defaultValue}
      placeholder={placeholder}
      fieldNames={{ label: 'name', value: 'code', children: 'items' }}
      className={'custom-dropdown ' + className}
      onChange={onChange as any}
      suffixIcon={
        suffixIcon || (
          <img
            src="/icons/expand-right.png"
            alt="expand-right"
            style={{
              width: 'auto',
              height: 'auto',
              transform: visiable ? 'rotate(-90deg)' : 'rotate(90deg)',
            }}
          />
        )
      }
      expandIcon={expandIcon || <img src="/icons/expand-right.png" alt="expand-right" />}
      onPopupVisibleChange={(visiable) => setVisiable(visiable)}
      options={options}
      allowClear={allowClear || false}
      dropdownRender={dropdownRender}
      {...props}
    />
  );
};

export default CustomDropdown;
