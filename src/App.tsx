import React, { memo } from 'react';
import { RootRouter } from './RouterContainer';
import { BrowserRouter as Router } from 'react-router-dom';
import { RecoilRoot } from 'recoil';
import firebase from 'firebase';
import { ConfigProvider as AntdFormConfigProvider } from 'antd';

import { firebaseConfig } from 'helpers';
import { registerServiceWorker } from 'serviceWorker';

import { validateMessages } from './utils';
import { QueryClient, QueryClientProvider } from 'react-query';

firebase.initializeApp(firebaseConfig);

registerServiceWorker();

if (firebase.messaging.isSupported()) {
  firebase
    .messaging()
    .requestPermission()
    .catch(() => {});
}

const queryClient = new QueryClient({
  defaultOptions: {
    mutations: {
      retryDelay: 0,
    },
    queries: {
      cacheTime: 0,
    },
  },
});

const App: React.SFC = memo(() => {
  return (
    <RecoilRoot>
      <QueryClientProvider client={queryClient}>
        <AntdFormConfigProvider form={{ validateMessages }}>
          <Router>
            <RootRouter />
          </Router>
        </AntdFormConfigProvider>
      </QueryClientProvider>
    </RecoilRoot>
  );
});

export default App;
