import { stringify } from 'qs';
import { useHistory, useLocation } from 'react-router-dom';
import useQueryString from './useQueryString';

export default function useNavigate<T>() {
  const params = useQueryString();
  const history = useHistory();
  const { pathname } = useLocation();

  return (updated: { [K in keyof T]?: T[K] }) => {
    history.push({
      pathname,
      search: stringify({ ...params, ...updated }),
    });
  };
}
