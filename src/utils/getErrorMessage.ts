export default (error: any) => {
  if (!error) return 'Oops! Something went wrong!';

  const ERROR = JSON.parse(JSON.stringify(error));

  console.log(ERROR);

  const { code } = ERROR;

  switch (code) {
    case 'ER_DUP_ENTRY':
      return '';

    default:
      return 'Oops! Something went wrong!';
  }
};
