import { message } from 'antd';

export { default as Color } from './color';
export { default as SeedAttribute } from './seedAttributeValue';

export const ErrorMessage = () => {
  message.error('Oops! Something went wrong!');
};

export const validateMessages = {
  required: 'Required',
  string: {
    // eslint-disable-next-line no-template-curly-in-string
    range: 'Please input ${min} - ${max} characters',
  },
  types: {
    email: 'Invalid email',
  },
};
